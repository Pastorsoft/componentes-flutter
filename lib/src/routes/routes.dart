import 'package:componentes_fernando/src/pages/card_page.dart';
import 'package:componentes_fernando/src/pages/control_page.dart';
import 'package:flutter/material.dart';

import 'package:componentes_fernando/src/pages/alert_page.dart';
import 'package:componentes_fernando/src/pages/avatar_page.dart';
import 'package:componentes_fernando/src/pages/home_page.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    '/': (BuildContext context) => HomePage(),
    'alert': (BuildContext context) => AlertPage(),
    'avatar': (BuildContext context) => AvatarPage(),
    'card': (BuildContext context) => CardPage(),
    'control': (BuildContext context) => ControlPage(),
  };
}
