import 'dart:io';

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:componentes_fernando/src/utils/icono_string_util.dart';

const data = {
  'opciones': [
    {
      'titulo': 'Inicio',
      'icon': 'home_rounded',
      'color': Color.fromRGBO(100, 100, 100, 0.2)
    },
    {
      'titulo': 'Fin',
      'icon': 'edit',
      'color': Color.fromRGBO(100, 100, 100, 0.2)
    },
    {
      'titulo': 'Control',
      'icon': 'search',
      'color': Color.fromRGBO(100, 100, 100, 0.2)
    },
    {
      'titulo': 'Ronda',
      'icon': 'play_arrow',
      'color': Color.fromRGBO(86, 94, 255, 0.4)
    },
  ]
};

class ControlPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Text('SafeCall 1.0.8'),
        actions: [
          Icon(
            Icons.dehaze,
            size: 40,
          ),
          SizedBox(
            width: 20,
          ),
        ],
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(vertical: 30),
        children: [
          Wrap(
            runSpacing: 25,
            alignment: WrapAlignment.spaceEvenly,
            children: _listasCard(),
          ),
          SizedBox(
            height: 300,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                margin: EdgeInsets.only(left: 15, right: 70),
                padding: EdgeInsets.symmetric(horizontal: 7),
                height: 40,
                child: Center(
                  child: Text(
                    'Servicio: FEFD',
                    style: TextStyle(fontSize: 18),
                  ),
                ),
                decoration: BoxDecoration(
                    border: Border.all(),
                    borderRadius:
                        BorderRadius.vertical(top: Radius.circular(10)),
                    color: Colors.cyan[100]),
              ),
              Container(
                margin: EdgeInsets.only(left: 25, right: 15),
                padding: EdgeInsets.symmetric(horizontal: 7),
                height: 40,
                child: Center(
                  child: Text(
                    'Tag: FE3D+00+009',
                    style: TextStyle(fontSize: 14),
                  ),
                ),
                decoration: BoxDecoration(
                    border: Border.all(),
                    borderRadius:
                        BorderRadius.vertical(top: Radius.circular(10)),
                    color: Colors.cyan[100]),
              ),
            ],
          ),
          Container(
            height: 100,
            color: Colors.grey[400],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                FlatButton(
                  height: 100,
                  minWidth: 120,
                  onPressed: () {
                    // launch('https://flutter.dev');
                    launch('tel://937749148');
                    print("llamandos");
                  },
                  child: Icon(
                    Icons.phone,
                    color: Colors.grey[800],
                    size: 40,
                  ),
                ),
                Container(
                    width: 170,
                    height: 100,
                    color: Colors.black,
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            _listaCard(),
                            _listaCard(),
                            _listaCard(),
                          ],
                        ),
                        Text(
                          'Securitas'.toUpperCase(),
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 25,
                              fontWeight: FontWeight.w800),
                        ),
                      ],
                    )),
                FlatButton(
                  minWidth: 115,
                  onPressed: null,
                  child: Text(''),
                  color: Colors.grey,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  List<Widget> _listasCard() {
    final List<Widget> opt = [];

    data['opciones'].forEach(
      (op) {
        final temp = _cardTipo3(
            getIcon(op['icon'], size: 60.0, colorIcon: Colors.white),
            op['titulo'],
            op['color']);
        opt.add(temp);
      },
    );
    return opt;
  }

  Widget _cardTipo3(Icon icono, String texto, Color colors) {
    return Container(
      width: 170,
      height: 150,
      child: FlatButton(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              width: 80,
              height: 80,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50), color: Colors.blue),
              //     BoxDecoration(borderRadius: BorderRadius.circular(20)),
              child: icono,
            ),
            Text(
              texto,
              style: TextStyle(fontSize: 22),
            ),
          ],
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
          side: BorderSide(
            color: Colors.black,
            width: 1.0,
          ),
        ),
        color: colors,
        onPressed: () {
          print(texto);
          print(NetworkInterface.list(includeLinkLocal: true));
          print(NetworkInterface.list().then(print));
          if (texto == "Control") {
            createAlbum(9);
            // launch("http://192.168.1.113:3000/control");
          }
        },
      ),
    );
  }

  Widget _listaCard() {
    return Card(
      child: Container(
        width: 40,
        height: 40,
      ),
      color: Colors.red[500],
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    );
  }

  createAlbum(int numero) async {
    var res = await http.post(
      'http://garciagaete.ddns.net:3000/control',
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode(<String, int>{
        'id': numero,
        'control': numero,
      }),
    );
    return res;
  }
}
