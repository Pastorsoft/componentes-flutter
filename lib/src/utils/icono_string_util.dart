import 'package:flutter/material.dart';

final _icons = <String, IconData>{
  'add_alert': Icons.add_alert,
  'accessibility': Icons.accessibility,
  'folder_open': Icons.folder_open,
  'album': Icons.album,
  'home_rounded': Icons.home_rounded,
  'edit': Icons.edit,
  'search': Icons.search,
  'play_arrow': Icons.play_arrow,
};

Icon getIcon(String nombreIcono, {double size, Color colorIcon = Colors.blue}) {
  return Icon(_icons[nombreIcono], color: colorIcon, size: size);
}
